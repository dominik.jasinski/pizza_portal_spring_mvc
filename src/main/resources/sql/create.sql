insert into pizza_type (id, name, description, price)
values
(1, "Wiejska", "kiełbasa, ser, pieczarki", 23),
(2, "Vegetarian", "mozarella, pomidory, bazylia, zioła", 19),
(3, "Kebab pizza", "kurczak,  cebula czerwona, ogórki kiszone", 22),
(4, "Serowa", "mozarella, ser feta, sera żółty", 20);


insert into pizza_size (id, name, extra_price)
values
(1, "Mała", 0),
(2, "Średnia", 3),
(3, "Duża", 6);


insert into pizza_topping (id, name, extra_price)
values
(1, "Podwójny ser", 2),
(2, "Podwójny ser mozzarella", 2),
(3, "Podwójny kurczak", 2),
(4, "Podwójne ciasto", 2),
(5, "Sos czosnkowy", 2),
(6, "Sos pomidorowy", 2);